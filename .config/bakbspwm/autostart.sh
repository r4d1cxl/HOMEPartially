#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}


run $HOME/.config/polybar/hack/launch.sh &

# run redshift-gtk &

# dex $HOME/.config/autostart/arcolinux-welcome-app.desktop
xsetroot -cursor_name left_ptr &
run sxhkd -c ~/.config/sxhkd/sxhkdrc &
run picom &
# conky -c $HOME/.config/bakbspwm/system-overview &
# run variety &
run nm-applet &
run Pamac-tray &
run xfce4-power-manager &
numlockx on &
blueberry-tray &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
run volumeicon &
nitrogen --restore &
