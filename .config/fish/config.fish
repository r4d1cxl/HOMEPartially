function fish_greeting; end

## ALIASES ##
alias upgrub="sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias jctl="journalctl -p 3 -xb"

alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'
alias getme="sudo pacman -S --noconfirm"
alias pacsy="sudo pacman -Syyy"
alias pacup="sudo pacman -Syyu --noconfirm"
alias yain="yay -S"
alias pacclean="sudo pacman -Rns (pacman -Qtdq)"
alias yayclean="yay -Scc"
alias neofetch="neofetch | lolcat"
alias mirror="sudo reflector -f 30 --latest 30 -a 6 --sort rate --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 -a 6 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 -a 6 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 -a 6 --sort age --save /etc/pacman.d/mirrorlist"
alias down="youtube-dl -x --audio-format mp3 --audio-quality 320k  -o '%(title)s.%(ext)s'"
alias yass="yay -Ss"
alias jn="jupyter-notebook"
### PROMPT DISPLAY ###
set -g theme_display_git yes
set -g theme_display_git_untracked yes
set -g theme_display_git_ahead_verbose yes
set -g theme_git_worktree_support yes
set -g theme_display_vagrant yes
set -g theme_display_docker_machine yes
set -g theme_display_hg yes
set -g theme_display_virtualenv yes
set -g theme_display_ruby no
set -g theme_display_user yes
set -g theme_display_vi no
set -g theme_display_date yes
set -g theme_display_cmd_duration yes
set -g theme_title_display_process yes
set -g theme_title_display_path yes
set -g theme_title_use_abbreviated_path no
set -g theme_date_format "+%a %H:%M"
set -g theme_avoid_ambiguous_glyphs yes
set -g theme_powerline_fonts yes
set -g theme_nerd_fonts yes
set -g theme_show_exit_status no
set -g default_user your_normal_user
set -g theme_color_scheme light
set -g fish_prompt_pwd_dir_length 0
set -g theme_project_dir_length 1
set fish_cursor_default     line    blink
set fish_cursor_insert      line       blink
set fish_cursor_replace_one underscore blink
set fish_cursor_visual      line

## PROMPT ##
colorscript random
